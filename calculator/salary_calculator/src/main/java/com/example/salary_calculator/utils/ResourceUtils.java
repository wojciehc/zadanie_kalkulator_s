package com.example.salary_calculator.utils;

import lombok.NonNull;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;

@Component
public class ResourceUtils {

    private static final String CLASSPATH = "classpath:";

    public File getFileResourceFile(@NonNull String filePath) throws FileNotFoundException {
        return org.springframework.util.ResourceUtils.getFile(CLASSPATH + filePath);
    }
}
