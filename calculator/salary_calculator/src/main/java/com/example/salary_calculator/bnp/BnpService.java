package com.example.salary_calculator.bnp;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

@Service
public class BnpService {

    protected static final String PLN = "PLN";

    private BnpUrlBuilder bnpUrlBuilder;
    private RestTemplate restTemplate;

    @Autowired
    public BnpService(BnpUrlBuilder bnpUrlBuilder, RestTemplate restTemplate) {
        this.bnpUrlBuilder = bnpUrlBuilder;
        this.restTemplate = restTemplate;
    }

    public BigDecimal getCurrencyRate(@NonNull String currencyCode) throws NullRateException {
        if (currencyCode.toUpperCase().equals(PLN)) {
            return BigDecimal.ONE;
        }
        String url = bnpUrlBuilder.build(currencyCode);
        ExchangeRatesResponse exchangeRatesResponse = restTemplate.getForObject(url, ExchangeRatesResponse.class);
        if (ExchangeRatesResponsePredicates.isNullOrEmpty().test(exchangeRatesResponse)) {
            throw new NullRateException();
        }
        return exchangeRatesResponse.getRates()[0].getMid();
    }
}
