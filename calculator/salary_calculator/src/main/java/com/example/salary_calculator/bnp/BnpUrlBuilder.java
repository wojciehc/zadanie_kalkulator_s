package com.example.salary_calculator.bnp;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Component
public class BnpUrlBuilder {

    protected static final String BASE_BNP_URL = "http://api.nbp.pl/";
    protected static final String API_EXCHANGERATES_RATES_A = "api/exchangerates/rates/a/";

    public String build(String currencyCode) {
        if (currencyCode == null) {
            return null;
        }
        return BASE_BNP_URL + API_EXCHANGERATES_RATES_A + currencyCode;
    }
}
