package com.example.salary_calculator.bnp;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
public class RateResponse {

    private String no;
    private Date effectiveDate;
    private BigDecimal mid;
}
