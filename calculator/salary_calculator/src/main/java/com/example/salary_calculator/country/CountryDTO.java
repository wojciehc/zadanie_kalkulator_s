package com.example.salary_calculator.country;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CountryDTO {

    private String name;
    private String countryCode;
    private String currencyCode;

    public CountryDTO(Country country) {
        if (country != null) {
            BeanUtils.copyProperties(country, this);
        }
    }
}
