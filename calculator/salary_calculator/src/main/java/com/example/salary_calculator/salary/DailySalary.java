package com.example.salary_calculator.salary;

import com.example.salary_calculator.utils.BigDecimalSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DailySalary {

    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal dailyGross;
    private String countryCode;
}
