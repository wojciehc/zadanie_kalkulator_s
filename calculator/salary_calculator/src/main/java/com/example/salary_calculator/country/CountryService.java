package com.example.salary_calculator.country;

import com.example.salary_calculator.utils.ResourceUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;


@Service
public class CountryService {

    @Autowired
    private ResourceUtils resourceUtils;

    public static final String DATA_COUNTRIES_JSON = "data/countries.json";

    protected Collection<CountryDTO> getCountryDTOs() throws IOException {
        return getCountries().stream().map(CountryDTO::new).collect(Collectors.toList());
    }

    protected Collection<Country> getCountries() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Country[] countries = objectMapper.readValue(resourceUtils.getFileResourceFile(DATA_COUNTRIES_JSON), Country[].class);
        return Arrays.asList(countries);
    }


    public Country getCountryByCode(@NonNull String countryCode) throws IOException, WrongCountryCodeException {
        Collection<Country> countries = getCountries();
        return countries.stream().filter(p -> p.getCountryCode().equals(countryCode))
                .findAny().orElseThrow(WrongCountryCodeException::new);
    }
}
