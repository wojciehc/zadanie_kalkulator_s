package com.example.salary_calculator.salary;

import com.example.salary_calculator.bnp.BnpService;
import com.example.salary_calculator.country.Country;
import com.example.salary_calculator.country.CountryService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class SalaryService {

    private final static BigDecimal DAYS_IN_MONTH = new BigDecimal(22);

    private final BnpService bnpService;
    private final CountryService countryService;

    @Autowired
    public SalaryService(CountryService countryService, BnpService bnpService) {
        this.countryService = countryService;
        this.bnpService = bnpService;
    }

    protected SalaryDTO calculateMonthlyNet(@NonNull DailySalary dailySalary) throws Exception {
        Country country = countryService.getCountryByCode(dailySalary.getCountryCode());
        BigDecimal currentRate = bnpService.getCurrencyRate(country.getCurrencyCode());
        BigDecimal calculated = calculate(currentRate, dailySalary.getDailyGross(), country);
        return new SalaryDTO(calculated);
    }

    private BigDecimal calculate(BigDecimal currentRate, BigDecimal dailyGross, Country country) {
        return ((dailyGross.multiply(DAYS_IN_MONTH)
                .multiply(BigDecimal.ONE.subtract(country.getVat())))
                .subtract(country.getFixedFee()))
                .multiply(currentRate)
                .multiply(BigDecimal.ONE.subtract(country.getTax()));
    }

}
