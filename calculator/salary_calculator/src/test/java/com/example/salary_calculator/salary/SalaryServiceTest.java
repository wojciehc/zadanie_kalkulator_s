package com.example.salary_calculator.salary;

import com.example.salary_calculator.bnp.BnpService;
import com.example.salary_calculator.country.Country;
import com.example.salary_calculator.country.CountryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(SpringRunner.class)
public class SalaryServiceTest {


    @Mock
    CountryService countryService;
    @Mock
    BnpService bnpService;

    @InjectMocks
    @Spy
    SalaryService salaryService;

    @Before
    public void setUp() throws Exception {
    }

    @Test(expected = NullPointerException.class)
    public void calculateMonthlyNetNull() throws Exception {
        salaryService.calculateMonthlyNet(null);
    }

    @Test(expected = NullPointerException.class)
    public void calculateMonthlyNetNullCountryCode() throws Exception {
        DailySalary dailySalary = new DailySalary(BigDecimal.ONE, null);
        salaryService.calculateMonthlyNet(dailySalary);
    }

    @Test(expected = NullPointerException.class)
    public void calculateMonthlyNetNullRate() throws Exception {
        doReturn(null).when(bnpService).getCurrencyRate(Mockito.anyString());
        DailySalary dailySalary = new DailySalary(BigDecimal.ONE, null);
        salaryService.calculateMonthlyNet(dailySalary);
    }


    @Test(expected = NullPointerException.class)
    public void calculateMonthlyNetNullDailyGross() throws Exception {
        DailySalary dailySalary = new DailySalary(null, "DE");
        salaryService.calculateMonthlyNet(dailySalary);
    }

    @Test
    public void calculateMonthlyNetSuccessPL() throws Exception {
        Country country = new Country("Polska", "PL", "PLN", BigDecimal.valueOf(1200),
                BigDecimal.valueOf(0.19), BigDecimal.valueOf(0.23));
        doReturn(country).when(countryService).getCountryByCode(Mockito.anyString());
        doReturn(BigDecimal.ONE).when(bnpService).getCurrencyRate(Mockito.anyString());


        DailySalary dailySalary = new DailySalary(BigDecimal.valueOf(500), "PL");
        SalaryDTO salaryDTO = salaryService.calculateMonthlyNet(dailySalary);

        assertThat(salaryDTO).isNotNull();
        assertThat(salaryDTO.getMonthlyNet().setScale(2, RoundingMode.HALF_UP))
                .isEqualTo(BigDecimal.valueOf(5888.70).setScale(2, RoundingMode.HALF_UP));
    }


}
