package com.example.salary_calculator.country;

import com.example.salary_calculator.utils.ResourceUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CountryServiceTest {

    @Mock
    ResourceUtils resourceUtils;

    @InjectMocks
    @Spy
    CountryService countryService;

    @Test(expected = NullPointerException.class)
    public void getCountriesNull() throws IOException {
        Mockito.when(resourceUtils.getFileResourceFile(Mockito.anyString())).thenThrow(new NullPointerException());

        countryService.getCountries();
    }

    @Test(expected = FileNotFoundException.class)
    public void getCountriesNotFound() throws IOException {
        Mockito.when(resourceUtils.getFileResourceFile(Mockito.anyString())).thenThrow(new FileNotFoundException());

        countryService.getCountries();
    }

    @Test
    public void getCountriesSuccess() throws IOException {
        Mockito.when(resourceUtils.getFileResourceFile(Mockito.anyString())).thenCallRealMethod();

        Collection<Country> countries = countryService.getCountries();

        assertThat(countries).isNotNull();
        assertThat(countries.size()).isEqualTo(3);
    }

    @Test(expected = NullPointerException.class)
    public void getCountryByCodeNull() throws Exception {
        countryService.getCountryByCode(null);
    }

    @Test(expected = WrongCountryCodeException.class)
    public void getCountryByCodeWrongCountryCode() throws Exception {
        setUpCountries();

        countryService.getCountryByCode("TEST");
    }

    @Test
    public void getCountryByCodeSuccess() throws Exception {
        setUpCountries();

        Country uk = countryService.getCountryByCode("UK");

        assertThat(uk).isNotNull();
        assertThat(uk.getName()).isEqualTo("Wielka Brytania");
        assertThat(uk.getCountryCode()).isEqualTo("UK");
        assertThat(uk.getCurrencyCode()).isEqualTo("GBP");
        assertThat(uk.getFixedFee()).isEqualTo("600");
        assertThat(uk.getTax()).isEqualTo(BigDecimal.valueOf(0.25));
        assertThat(uk.getVat()).isEqualTo(BigDecimal.valueOf(0.2));
    }

    @Test(expected = NullPointerException.class)
    public void getCountryDTOsNull() throws Exception {
        Mockito.doThrow(new NullPointerException()).when(countryService).getCountries();

        countryService.getCountryDTOs();
    }

    @Test(expected = FileNotFoundException.class)
    public void getCountryDTOsNotFound() throws Exception {
        Mockito.doThrow(new FileNotFoundException()).when(countryService).getCountries();

        countryService.getCountryDTOs();
    }

    @Test
    public void getCountryDTOsSuccess() throws Exception {
        setUpCountries();

        Collection<CountryDTO> dtos = countryService.getCountryDTOs();

        assertThat(dtos).isNotNull();
        assertThat(dtos.size()).isEqualTo(2);
        assertThat(dtos.contains(new CountryDTO("Polska", "PL", "PLN"))).isTrue();
        assertThat(dtos.contains(new CountryDTO("Wielka Brytania", "UK", "GBP"))).isTrue();
    }

    private void setUpCountries() throws IOException {
        Country countryUK = new Country("Wielka Brytania", "UK", "GBP", BigDecimal.valueOf(600), BigDecimal.valueOf(0.25), BigDecimal.valueOf(0.2));
        Country countryPL = new Country("Polska", "PL", "PLN", BigDecimal.valueOf(1200), BigDecimal.valueOf(0.19), BigDecimal.valueOf(0.23));
        doReturn(Arrays.asList(countryPL, countryUK)).when(countryService).getCountries();
    }
}
