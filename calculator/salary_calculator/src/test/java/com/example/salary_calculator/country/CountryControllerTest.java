package com.example.salary_calculator.country;

import com.example.salary_calculator.salary.SalaryController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CountryControllerTest {

    private static final String TEST_GET_COUNTRIES_URL = "/countries";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CountryService countryService;
    @MockBean
    private SalaryController salaryService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getCountriesNull() throws Exception {
        Mockito.doThrow(new NullPointerException()).when(countryService).getCountryDTOs();

        mockMvc.perform(MockMvcRequestBuilders.get(TEST_GET_COUNTRIES_URL))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getCountriesNotFound() throws Exception {
        Mockito.doThrow(new FileNotFoundException()).when(countryService).getCountryDTOs();

        mockMvc.perform(MockMvcRequestBuilders.get(TEST_GET_COUNTRIES_URL))
                .andDo(print())
                .andExpect(status().isInternalServerError());
    }


    @Test
    public void getCountriesSuccess() throws Exception {
        CountryDTO countryPL = new CountryDTO("Polska", "PL", "PLN");
        CountryDTO countryDE = new CountryDTO("Niemcy", "DE", "EUR");
        List<CountryDTO> countries = Arrays.asList(countryPL, countryDE);
        when(countryService.getCountryDTOs()).thenReturn(countries);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(TEST_GET_COUNTRIES_URL))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        String body = mvcResult.getResponse().getContentAsString();

        assertThat(body.contains("Polska")).isTrue();
        assertThat(body.contains("Niemcy")).isTrue();
    }
}
