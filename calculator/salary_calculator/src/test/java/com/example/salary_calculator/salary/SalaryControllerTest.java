package com.example.salary_calculator.salary;

import com.example.salary_calculator.bnp.NullRateException;
import com.example.salary_calculator.country.CountryController;
import com.example.salary_calculator.country.WrongCountryCodeException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.math.BigDecimal;

import static com.example.salary_calculator.exception.GlobalExceptionHandler.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest
public class SalaryControllerTest {

    private static final String TEST_POST_SALARY_URL = "/salary/calculate-polish";
    private static final String ERROR_MESSAGE = "error.message";


    @SpyBean
    SalaryController salaryController;

    @MockBean
    CountryController countryController;

    @MockBean
    private SalaryService salaryService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void calculatePolishSalarySuccess() throws Exception {
        DailySalary dailySalary = new DailySalary(new BigDecimal(50), "DE");

        when(salaryService.calculateMonthlyNet(any(DailySalary.class))).thenReturn(new SalaryDTO(BigDecimal.TEN));

        mockMvc.perform(MockMvcRequestBuilders.post(TEST_POST_SALARY_URL)
                .contentType(APPLICATION_JSON_UTF8)
                .content(objectToJson(dailySalary)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("monthlyNet").value("10.00"));
    }


    @Test
    public void calculatePolishSalaryBadRequest() throws Exception {
        performPostTest(HttpMessageNotReadableException.class, status().isBadRequest(), ERROR_REQUEST);

    }

    @Test
    public void calculatePolishSalaryWrongCountryCode() throws Exception {
        performPostTest(WrongCountryCodeException.class, status().isBadRequest(), ERROR_REQUEST);
    }

    @Test
    public void calculatePolishSalaryNullRate() throws Exception {
        performPostTest(NullRateException.class, status().isBadRequest(), ERROR_RESPONSE_FROM_REMOTE_SERVICE);
    }

    @Test
    public void calculatePolishSalaryNull() throws Exception {
        performPostTest(NullPointerException.class, status().isBadRequest(), ERROR_REQUEST);
    }

    @Test
    public void calculatePolishSalaryClientError() throws Exception {
        performPostTest(HttpClientErrorException.class, status().isBadRequest(), ERROR_RESPONSE_FROM_REMOTE_SERVICE);
    }

    @Test
    public void calculatePolishSalaryErrorConnection() throws Exception {
        performPostTest(ResourceAccessException.class, status().isRequestTimeout(), ERROR_CONNECTION_WITH_REMOTE_SERVER);
    }

    @Test
    public void calculatePolishSalaryNoHandlerFound() throws Exception {
        performPostTest(NoHandlerFoundException.class, status().isBadRequest(), ERROR_REQUEST);
    }

    @Test
    public void calculatePolishSalaryRemoteError() throws Exception {
        performPostTest(RestClientException.class, status().isBadRequest(), ERROR_RESPONSE_FROM_REMOTE_SERVICE);
    }


    private void performPostTest(Class<? extends Exception> exception, ResultMatcher status, String message) throws Exception {
        DailySalary dailySalary = new DailySalary();
        doThrow(exception).when(salaryService).calculateMonthlyNet(dailySalary);

        mockMvc.perform(MockMvcRequestBuilders.post(TEST_POST_SALARY_URL)
                .contentType(APPLICATION_JSON_UTF8)
                .content(objectToJson(dailySalary)))
                .andDo(print())
                .andExpect(status)
                .andExpect(jsonPath(ERROR_MESSAGE).value(message));
    }

    private String objectToJson(DailySalary dailySalary) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(dailySalary);
    }

}
