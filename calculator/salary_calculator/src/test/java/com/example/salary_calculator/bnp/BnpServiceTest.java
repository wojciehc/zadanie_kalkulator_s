package com.example.salary_calculator.bnp;

import com.example.salary_calculator.salary.DailySalary;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BnpServiceTest {

    @Mock
    BnpUrlBuilder bnpUrlBuilder;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    @Spy
    BnpService bnpService;

    @Test(expected = NullPointerException.class)
    public void getCurrencyRateNull() throws NullRateException {
        bnpService.getCurrencyRate(null);
    }


    @Test(expected = NullRateException.class)
    public void getCurrencyRateNullRate() throws NullRateException {
        when(bnpUrlBuilder.build(anyString())).thenReturn("http://api.nbp.pl/...");

        when(restTemplate.getForObject(anyString(), (Class<Object>) any())).thenReturn(null);

        bnpService.getCurrencyRate("RUS");
    }


    @Test
    public void getCurrencyRateSuccessForeignCountry() throws NullRateException {
        RateResponse rateResponse = new RateResponse("a", new Date(), BigDecimal.valueOf(3.25));
        RateResponse[] rates = {rateResponse};
        ExchangeRatesResponse exchangeRatesResponse =
                new ExchangeRatesResponse("a", "no", "03-07-2019", rates);
        when(bnpUrlBuilder.build(anyString())).thenReturn("http://api.nbp.pl/...");

        when(restTemplate.getForObject(anyString(), (Class<Object>) any()))
                .thenReturn(exchangeRatesResponse);

        BigDecimal rate = bnpService.getCurrencyRate("DE");

        assertThat(rate).isEqualTo(BigDecimal.valueOf(3.25));
    }

    @Test
    public void getCurrencyRateSuccessPL() throws NullRateException {
        BigDecimal rateUppercase = bnpService.getCurrencyRate("PLN");
        BigDecimal rate = bnpService.getCurrencyRate("pln");

        assertThat(rateUppercase).isEqualTo(BigDecimal.valueOf(1));
        assertThat(rate).isEqualTo(BigDecimal.valueOf(1));
    }

}
